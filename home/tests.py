from django.test import TestCase,Client

# Create your tests here.
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import home

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class LandingPageTest(TestCase):
    def test_apakah_ada_url_landing_page(self):
        response= Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_tidak_ada_url_landing_page(self):
        response= Client().get('/other_url')
        self.assertEqual(response.status_code, 404)

    def test_apakah_ada_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_fungsi_landing_page(self):
        found= resolve('/')
        self.assertEqual(found.func, home)
    
    def test_ada_accordion(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('id="accordion"', content)

    def test_ada_aktivitas(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Aktivitas', content)

    def test_ada_organisasi_kepanitiaan(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Pengalaman Organisasi/Kepanitiaan', content)

    def test_ada_prestasi(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Prestasi', content)

    def test_ada_slider_switch_dark_mode(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('switch', content)
        self.assertIn('slider', content)
        self.assertIn('Change theme', content)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(executable_path='./chromedriver', chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()
    
    def test_change_theme(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        time.sleep(4)
        button = driver.find_element_by_id("slider_theme")
        button.click()
        time.sleep(4)
        body = driver.find_element_by_class_name("theme")
        # tulisan = driver.find_element_by_name("isi-tulisan")
        # assert "dark" in tulisan.get_attribute("class")
        assert "dark_theme" in body.get_attribute("class")
        time.sleep(4)

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    

